#!/bin/bash
# 下载elasticsearch 镜像
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.2.0

# 下载kibana 镜像
docker pull docker.elastic.co/kibana/kibana:7.2.0

# 创建 bigdata 命名空间
kubectl create ns bigdata

# 创建es pv yaml
cat << EOF > k8s-pv-es.yaml
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: k8s-pv-es1
  namespace: bigdata
  labels:
    type: local
spec:
  storageClassName: es
  capacity:
    storage: 15Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: /data/k8s/mnt/es/es1
  persistentVolumeReclaimPolicy: Recycle
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: k8s-pv-es2
  namespace: bigdata
  labels:
    type: local
spec:
  storageClassName: es
  capacity:
    storage: 15Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: /data/k8s/mnt/es/es2
  persistentVolumeReclaimPolicy: Recycle
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: k8s-pv-es3
  namespace: bigdata
  labels:
    type: local
spec:
  storageClassName: es
  capacity:
    storage: 15Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: /data/k8s/mnt/es/es3
  persistentVolumeReclaimPolicy: Recycle
EOF

# 创建 es cluste yaml 文件
cat << EOF > k8s-es-cluster.yaml
---
kind: Service
apiVersion: v1
metadata:
  name: es
  namespace: bigdata
  labels:
    app: elasticsearch
spec:
  selector:
    app: elasticsearch
  type: NodePort
  ports:
    - port: 9200
      nodePort: 30080
      name: rest
    - port: 9300
      nodePort: 30070
      name: inter-node
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: es-cluster
  namespace: bigdata
spec:
  serviceName: es
  replicas: 3
  selector:
    matchLabels:
      app: elasticsearch
  template:
    metadata:
      labels:
        app: elasticsearch
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: "app"
                    operator: In
                    values:
                      - elasticsearch
                  - key: "kubernetes.io/hostname"
                    operator: NotIn
                    values:
                      - master
              topologyKey: "kubernetes.io/hostname"
      containers:
        - name: elasticsearch
          image: docker.elastic.co/elasticsearch/elasticsearch:7.2.0
          imagePullPolicy: IfNotPresent
          resources:
            limits:
              cpu: 1000m
            requests:
              cpu: 100m
          ports:
            - containerPort: 9200
              name: rest
              protocol: TCP
            - containerPort: 9300
              name: inter-node
              protocol: TCP
          volumeMounts:
            - name: data
              mountPath: /usr/share/elasticsearch/data
          env:
            - name: cluster.name
              value: k8s-logs
            - name: node.name
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: discovery.seed_hosts
              value: "es-cluster-0.es,es-cluster-1.es,es-cluster-2.es"
            - name: cluster.initial_master_nodes
              value: "es-cluster-0,es-cluster-1,es-cluster-2"
            - name: ES_JAVA_OPTS
              value: "-Xms512m -Xmx512m"
      initContainers:
        - name: fix-permissions
          image: busybox
          imagePullPolicy: IfNotPresent
          command: ["sh", "-c", "chown -R 1000:1000 /usr/share/elasticsearch/data"]
          securityContext:
            privileged: true
          volumeMounts:
            - name: data
              mountPath: /usr/share/elasticsearch/data
        - name: increase-vm-max-map
          image: busybox
          imagePullPolicy: IfNotPresent
          command: ["sysctl", "-w", "vm.max_map_count=262144"]
          securityContext:
            privileged: true
        - name: increase-fd-ulimit
          image: busybox
          imagePullPolicy: IfNotPresent
          command: ["sh", "-c", "ulimit -n 65536"]
          securityContext:
            privileged: true
  volumeClaimTemplates:
    - metadata:
        name: data
        labels:
          app: elasticsearch
      spec:
        accessModes: [ "ReadWriteMany" ]
        storageClassName: es
        resources:
          requests:
            storage: 10Gi
---
apiVersion: v1
kind: Service
metadata:
  name: kibana
  namespace: bigdata
  labels:
    app: kibana
spec:
  type: NodePort
  ports:
    - port: 5601
      nodePort: 30090
      targetPort: 5601
  selector:
    app: kibana
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kibana
  namespace: bigdata
  labels:
    app: kibana
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kibana
  template:
    metadata:
      labels:
        app: kibana
    spec:
      containers:
        - name: kibana
          image: docker.elastic.co/kibana/kibana:7.2.0
          imagePullPolicy: IfNotPresent
          resources:
            limits:
              cpu: 1000m
            requests:
              cpu: 100m
          env:
            - name: ELASTICSEARCH_HOSTS
              value: http://es:9200
          ports:
            - containerPort: 5601
EOF

# 创建es pv pod
kubectl apply -f k8s-pv-es.yaml -n bigdata

# 创建es cluster pod
kubectl apply -f k8s-es-cluster.yaml
